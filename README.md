# Markdown Directory Listing

Creates a new README.md file with the project name, description, and a list of markdown files in the current and sub directories.

(The previus README.md is moved to README_previus.md)

## To run

Install globally, or run:

```sh
npx markdown_file_list@latest
# or...
npm markdown_file_list@latest home.md
npm markdown_file_list@latest directory.md
``` 
...in the location of the target README.md file. (ex: The basepath of the documentation project.)