module.exports = {
  'env': {
    'es2021': true,
    'node': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 12,
  },
  'plugins': [
    '@typescript-eslint'
  ],
  'rules': {
    'semi': ['error', 'always'],
    'indent': ['error', 2],
    '@typescript-eslint/no-explicit-any': 'off',
    'quotes': ['warn','single'],
    'no-unused-vars': ['warn'],
  }
};
