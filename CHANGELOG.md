# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.5.3](https://gitlab.com/mechkit/markdown_file_list/compare/v0.5.2...v0.5.3) (2021-12-20)


### Bug Fixes

* better husky config ([e464ef9](https://gitlab.com/mechkit/markdown_file_list/commit/e464ef9f3313d34a7228f46101a82ee8b062bd93))

### [0.5.2](https://gitlab.com/mechkit/markdown_file_list/compare/v0.5.1...v0.5.2) (2021-12-20)


### Bug Fixes

* updated readme. ([61e33fc](https://gitlab.com/mechkit/markdown_file_list/commit/61e33fccb2f77376ba5e28b999b3d8aacca62d8e))

### [0.5.1](https://gitlab.com/mechkit/markdown_file_list/compare/v0.5.0...v0.5.1) (2021-12-20)


### Features

* added input for filename of the output markdown ([001a5a7](https://gitlab.com/mechkit/markdown_file_list/commit/001a5a73518f44951d9b49bde63d928778d012f3))
* setup linting of code and commits ([d44cb0f](https://gitlab.com/mechkit/markdown_file_list/commit/d44cb0f1ee2f811d23c842e897616423de64020f))
