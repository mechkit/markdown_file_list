#!/usr/bin/env node
/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs');
const path = require('path');
const ignore = require('ignore');
const config = require('../package.json');
// print process.argv
/*
process.argv.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
});
*/
const to_display_name = function(var_name) {
  let display_name = var_name.split('_').map( word =>{
    return word[0].toUpperCase() + word.slice(1);
  }).join(' ');
  return display_name;
};

const source_dir = process.cwd();
const output_file_name = process.argv[2] || 'README.md';
const output_path = path.join(source_dir, output_file_name);
const gitignore_path = '.gitignore';

console.log('Markdown Directory Listing');
console.log('version:',config.version);
console.log('---');

let repo_name;
let description = false;
try {
  const config = require( path.join( source_dir, 'package.json') );
  if ( config && config.description ) description = config.description;
  if ( config && config.name ) repo_name = to_display_name(config.name);
} catch (error) {
  console.log('      (No package.json)');
}
repo_name = repo_name || to_display_name(source_dir.split('/').pop() );

console.log('for:',repo_name);
console.log('from:', source_dir);
console.log('  to:', output_path);

let gitignore_string = '';
try {
  fs.accessSync(gitignore_path, fs.constants.R_OK | fs.constants.W_OK);
  // .add(fs.readFileSync('.gitignore').toString());
  // file_lines = fs.readFileSync(gitignore_path, { encoding: 'utf8', flag: 'r' }).split('\n');
  gitignore_string = fs.readFileSync(gitignore_path, { encoding: 'utf8', flag: 'r' }).toString();
  console.error('      (Loading .gitignore)');
} catch (err) {
  console.error('      (no .gitignore)');
}

console.log('---');

const ig = ignore()
  .add(output_file_name)
  .add('.git')
  .add(gitignore_string);

const is_md = file => /.md$/.test(file);
// const remove_extension = file => file.split('.').slice(0, -1).join('.');
/*
const toPascalCase = string =>
  string
    .match(/[a-z]+/gi)
    //.split(/[_-]/gi)
    .map(word => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase())
    .join('');
//*/
let re_special = /^#+|^ *\*|^ *\+|^ *-|^ *[0-9]+\.|^-{3}/;

const get_file_list = function (base_path, sub_path) {
  let files = {};
  sub_path = sub_path || '';
  const target_dir = path.join(base_path, sub_path);
  fs.readdirSync(target_dir)
    // .filter(is_md)
    .forEach(x_path => {
      const target_path = path.join(base_path, sub_path, x_path);
      const target_path_relative = path.join(sub_path, x_path);
      if (ig.ignores(target_path_relative)) {
        return files;
      } else if (fs.statSync(target_path).isDirectory()) {
        let dir_name = x_path;
        files[dir_name] = files[dir_name] || {};
        files[dir_name] = Object.assign(files[dir_name], {
          // type: 'dir',
          name: dir_name,
          children: get_file_list(base_path, path.join(sub_path, x_path)),
        });
        if ( Object.keys(files[dir_name].children).length === 0 ) delete files[dir_name];
      } else if (is_md(target_path)) {
        let file_name = x_path;
        const lines = fs.readFileSync(target_path, { encoding: 'utf8', flag: 'r' }).split('\n');
        const first_line = lines[0];
        const second_line = lines[1];
        let file_parts = file_name.split('.'); // catergory.sub_cat.subject.md --> ['category','sub_cat','subject','md']
        file_parts.pop(); // .pop() assumes only *.md files make it this far   //  ['category','sub_cat','subject']
        let target = files; // set target directory, to be altered below.
        while ( file_parts.length > 1 ) { // if there is structure beyond filename. ex: tech.node.config.md
          let new_sub = file_parts.shift(); // grab the top level structure. ex: 'tech' (then 'node' on next loop)
          target[new_sub] = target[new_sub] || {}; // build structure into files
          target[new_sub].children = target[new_sub].children || {};
          target = target[new_sub].children; // move target down to the new level
        }
        let subject_name = file_parts[0]; // the last part (or only part) is the subject_name. ex from above: 'config'
        let title = to_display_name(subject_name);
        let description;
        if (first_line !== undefined && first_line[0] === '#') {
          title = first_line.replace(/#/g, '').trim();
          let special_match = second_line.trim().match(re_special);
          if ( ! special_match && second_line.trim() !== '' ) {
            description = second_line.trim();
          }
        }
        target[subject_name] = target[subject_name] || {};
        target[subject_name] = Object.assign(target[subject_name], {
          // type: 'file',
          name: subject_name,
          title,
          description,
          sub_path: sub_path.replace(/\\/g, '/'), // windows fix
          file_name,
        });
      }
    });
  return files;
};

// getting all the files
const markdown_file_list = get_file_list(source_dir);
// console.log(markdown_file_list);

// auto add icon files that are not in the table above
let output_lines = [];

output_lines.push('# '+repo_name);
if ( description ) output_lines.push(description);
output_lines.push('');
output_lines.push('## File directory');

const files_to_markdown_lines = function(lines, files, indent ) {
  lines = lines || [];
  indent = indent || '';
  Object.keys(files).forEach( name => {
    let o = files[name];
    if ( o.children !== undefined ) { // is a directory
      if ( o.file_name ) {
        let file_path = path.join(o.sub_path, o.file_name);
        lines.push( `${indent}- [${ o.title }](${ file_path }):`);
      } else {
        lines.push( `${indent}- ${name}:` );
      }
      lines = files_to_markdown_lines(lines, o.children, indent+'  ');
    } else { // is a file
      let file = o;
      let file_path = path.join(file.sub_path, file.file_name);
      let markdown_line = `${indent}- [${ file.title }](${ file_path })`;
      if ( file.description !== undefined ) {
        markdown_line += ': ' + file.description;
      }
      lines.push( markdown_line );
    }
    return lines;
  });

  return lines;
};

output_lines = files_to_markdown_lines(output_lines,markdown_file_list);
console.log('---');
console.log('DONE');
// console.log(output_lines);

fs.writeFileSync(
  output_path,
  output_lines.join('\n'),
);